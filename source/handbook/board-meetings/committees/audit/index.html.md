---
layout: handbook-page-toc
title: "Audit Committee"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Audit Committee Composition

* **Chairperson:** Karen Blasing
* **Members:** Bruce Armstrong, David Hornik
* **Management DRI:** Chief Financial Officer

## Audit Committee Charter

1.	Purpose. The purpose of the Audit Committee of the Board of Directors (the “Board”) of GitLab Inc. (the “Company”) is to assist the Board’s oversight of:
    - The integrity of the Company’s financial statements;
    - The performance, qualifications and independence of the Company’s registered public accounting firm (the “external auditors”);
    - The performance of the Company’s internal financial, accounting and reporting controls and other processes.
    - The company's process for monitoring compliance with laws and regulations and the code of conduct.

1.	Structure and Membership
    - Members. The Audit Committee shall consist of at least two members of the Board, each of whom shall be independent.
    - Financial Literacy. Each member of the Audit Committee must be financially literate, as such qualification is interpreted by the Board in its business judgment, or must become financially literate within a reasonable period of time after his or her appointment to the Audit Committee.
    - Chair. Unless the Board elects a Chair of the Audit Committee, the Audit Committee shall elect a Chair by majority vote.
    - Selection and Removal. Members of the Audit Committee shall be appointed by the Board.
1.	Authority and Responsibilities
    - General. The Audit Committee shall discharge its responsibilities, and shall assess the information provided by the Company’s management and the external auditors, in accordance with its business judgment. Management is responsible for the preparation, presentation, and integrity of the Company’s financial statements and for the appropriateness of the accounting principles and reporting policies that are used by the Company. The external auditors are responsible for auditing the Company’s financial statements. The authority and responsibilities set forth in this Charter do not reflect or create any duty or obligation of the Audit Committee to plan or conduct any audit, to determine or certify that the Company’s financial statements are complete, accurate, fairly presented, or in accordance with generally accepted accounting principles or applicable law, or to guarantee the external auditors’ reports.
    - Oversight of Integrity of Financial Statements
    - Review and Discussion. The Audit Committee shall meet to review and discuss with the Company’s management and external auditors the Company’s audited financial statements.
    - Related-Person Transactions. The Audit Committee shall review related-person transactions under the Company’s Related Person Transaction Policy and applicable accounting standards on an ongoing basis and such transactions shall be approved by the Audit Committee.
    - Oversight of Performance, Qualification and Independence of External Auditors
    - Consider the effectiveness of the company's internal control system, including information technology security and control.
    - Understand the scope of internal and external auditors' review of internal control over financial reporting, and obtain reports on significant findings and recommendations, together with management's responses.
    - Review fraud risk assessment of the entity
    - Approve the internal audit charter
    - Approve the annual audit plan and all major changes to the plan. Review the internal audit activity’s performance relative to its plan.
    - Review the effectiveness of the system for monitoring compliance with laws and regulations and the results of management's investigation and follow-up (including disciplinary action) of any instances of noncompliance.
    - Review the findings of any examinations by regulatory agencies, and any auditor observations.
    - Review the process for communicating the code of conduct to company personnel, and for monitoring compliance therewith.
    - Obtain regular updates from management and company legal counsel regarding compliance matters.
    - Review and assess the adequacy of the audit committee charter annually, requesting board approval for proposed changes, and ensure appropriate disclosure as may be required by law or regulation.
    - Review new accounting standards- impact and implementation plan, code of conduct violations including hotline complaints , cybersecurity risk assessment conducted by management , Key contracts and IT initiatives taken by management



1.	Selection. The Audit Committee shall be responsible for appointing, evaluating and, when necessary, terminating the engagement of the external auditors. The Audit Committee may, in its discretion, seek stockholder ratification of the external auditors it appoints.
1.	Independence. The Audit Committee shall assist the Board in its assessment of the independence of the external auditors. In connection with this assessment, the Audit Committee shall, at least annually, obtain and review a report from the external auditors describing relationships between the external auditors and the Company, including the disclosures required by the applicable requirements of the Public Company Accounting Oversight Board regarding the external auditors’ independence. The Audit Committee shall actively engage in dialogue with the external auditors concerning any disclosed relationships or services that might impact the objectivity and independence of the external auditors.
1.	Compensation. The Audit Committee shall be directly responsible for setting the compensation of the external auditors. The Audit Committee is empowered, without further action by the Board, to cause the Company to pay the compensation of the external auditors established by the Audit Committee.
1.	Oversight. The external auditors shall report directly to the Audit Committee and the Audit Committee shall be directly responsible for overseeing the work of the external auditors, including resolution of disagreements between Company management and the external auditors regarding financial reporting.
1.	Procedures and Administration
    - Meetings. The Audit Committee shall meet in person or telephonically as often as it deems necessary in order to perform its responsibilities. The Audit Committee may also act by unanimous written consent in lieu of a meeting. The Audit Committee shall periodically meet separately with: (i) the external auditors and (ii) Company management. The Audit Committee shall keep minutes of its meetings and provide those to the Board of Directors.
1. 	Independent Advisors. The Audit Committee shall have the authority, without further action by the Board, to engage and determine funding for such independent legal, accounting and other advisors as it deems necessary or appropriate to carry out its responsibilities. Such independent advisors may be the regular advisors to the Company. The Audit Committee is empowered, without further action by the Board, to cause the Company to pay the compensation of such advisors as established by the Audit Committee.
1.	Investigations. The Audit Committee shall have the authority to conduct or authorize investigations into any matter within the scope of its responsibilities, as it shall deem appropriate, including the authority to request any officer, employee or advisor of the Company to meet with the Audit Committee or any advisors engaged by the Audit Committee.
1.	Additional Powers. The Audit Committee shall have such other duties as may be delegated.

### Audit Committee Agenda Planner

We review the below topics no less frequently than the following schedule:


#### Management, Accounting and Reporting

|  Topics                                                                               | FY Q1 | FY Q2  |  FY Q3  |  FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Accounting policies                                                                        |    |  X   |           |  X     |
| Significant estimates and judgements                                 |          |           |     | X|
| New accounting standards – impact and implementation plan            |        X |    X       | X | X      |
| Review of financial Statements                                       |        X   |           |   |       |
| Related party transactions                                           |          |           |   |  X    |
| Treasury                                                             |          |           |   |  X    |
| ERM – Review of financial statement  risk factors                    | X        |           |  |      |
| Insurance coverage update                                            |          |           |   | X   |
| Close process                                                        |        X |            |   |        |
| Stock transactions                                                   |        X |            |   |        |
| Tax audits / Taxes                                                   |          |            |   | X      |
| Public reporting (GAAP and Non-GAAP financials, non-GAAP metrics)   |        X |            |   |        |
| Guidance model                                                       |        X |            |   |        |

#### People Division

| Topics                          | FY Q1  |   FY Q2  |  FY Q3 | FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Global staffing update, succession plan and continuous improvement   |     X    |           |   |       |
| EEO audits                                                           |     X    |           |   |       |
| Payroll                                                              |          |      X    |   |       |
| Compensation and hiring                                              |     X    |           |   |       |

#### Legal, Risk and Compliance

| Topics                          | FY Q1  |   FY Q2  |  FY Q3 | FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Compliance to business conduct (including hotline complaints and code of conduct violations) |    X    |           | |     |
| ERM – Risk assessment updates                              |    X      |     |       |       |
| Regulatory compliance  |          |   |    X  |      |
| Privacy                |          | X |       |      |
| Reg FD  - Fair Disclosure  |     X     |   |      |      |
| Reg G - Governance        |      X   |   |      |      |
| Committee annual assessment  |          | X  |      |      |

#### Security Compliance

| Topics                          | FY Q1  |   FY Q2  |  FY Q3 | FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| ERM – Cyber risk assessment         |     X    |         |    |      |
| Cybersecurity                                    |         |     X      |  |      |
| Application system reviews:<br><br> Tech Stack - for accounting function & GCF handbook<br> 1. Accounting - Netsuite<br> 2. Tax - Avalara<br> 3. Stock - Carta<br> 4. Planning - NA<br>  5. HR - Bamboo HR<br>  6. License provisioning - Zuora<br> 7. Commission    system - Captivate                                      |     X     |           |   |     |
| IT implementation projects and initiatives  |     X    |   |      |      |
| IT security update              |     X    |   |      |      |

#### Internal Audit

| Topics                          | FY Q1 | FY Q2 |  FY Q3  |  FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Internal audit and global annual plan                           |                 |        |           |   X   |
| Internal audit activity report and annual plan update            |         X         | X         |     X      |   X  |
| SOX - Internal control over financial reporting assessment and deficiencies status update  |    X | X      | X     |  X       |
| Internal controls (pre-Sox)                                    |         |   X     |   |       |
| Internal audit charter review                                     |     X    |        |   |       |
| Fraud Risk assessment                                    |      X    |           |   X  |  |
| Annual assessment of internal audit   |          |       X    |     X    |  |

#### External Audit

| Topics                          | FY Q1 | FY Q2  |   FY Q3  |  FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Global audit plan and fees/Appoint External Auditor |  | | X |X |
| Year-end audit results and required communications, as applicable |         |   X    |   |    X    |
| Annual assessment of audit firm, engagement team and lead audit partner                      |          | |    X    |       |
| Independence review | | | X| X |
| Audit               |X| |  |   |

#### General

| Topics                          | FY Q1 | FY Q2  |  FY Q3  |  FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Executive session - as needed  | X | X | X |X |
| Approval of minutes                                        |X  | X     | X        |X      |
| Committee annual assessment                            |          |    X       |      | |
| Closed session as needed         |X |    X | X     |X         |
