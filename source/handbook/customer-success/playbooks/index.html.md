---
layout: handbook-page-toc
title: "Customer Success Playbooks"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [Customer Success homepage](/handbook/customer-success/) for additional Customer Success handbook content.

---

# Playbooks

The following page provides access to the Customer Success playbooks to assist in the process of selling, driving adoption, and ultimately delivering more value to customer via GitLab's DevOps platform. These playbooks will include original content as well as links to other parts of the handbook.

## Structure

**Procedure**: Outline the steps to discover, position, lead value discussions, and drive adoption (TAM only). The will be driven and tracked [via Gainsight](https://about.gitlab.com/handbook/customer-success/using-gainsight-within-customer-success/).

**Positioning**: Stage and use case value proposition and positioning with supporting collateral. 

- Market requirements, personas, and differentiators 
- Top features, stages, and categories
- Competitive assessments 
- Customer slides
- Proof points, blogs, and customer success stories

**Discovery**: Discussion tools (e.g., discovery questions) using Command Plan approach. 

**Adoption**: Adoption-related reference materials to accelerate adoption. 

- Adoption map, noting recommend recommended features / use cases and sequence of adoption (where applicable)
- Telemetry attributes to track adoption
- Product documentation (Content owner: Product and Engineering Teams)
- Enablement and training assets
- Paid services

## Catalogue of Playbooks

The following playbooks are aligned to our [customer adoption journey](https://about.gitlab.com/handbook/customer-success/vision/#high-level-visual-of-gitlab-adoption-journey) and support adoption of the related customer capability and [GitLab stage](https://about.gitlab.com/handbook/product/categories/) .

##### [Source Code Management (SCM) / Create Stage](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/version-control-collaboration/)
##### [Continous Integration / Verify](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/ci/)
##### [Continuous Delivery / Release](https://about.gitlab.com/handbook/customer-success/playbooks/cd-release.html) *(WIP)*
##### [DevSecOps / Security / Secure](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/devsecops/)

## TAM Playbooks

TAM's create playbooks to provide a prescriptive methodology to help with customer discussions around certain aspects of GitLab.

- [Stage Adoption Guideline](https://about.gitlab.com/handbook/customer-success/tam/stage-adoption/) *(will be depricated when adoption maps merged into the use case pages)*
- [TAM CI Workshop](https://about.gitlab.com/handbook/customer-success/playbooks/ci-verify.html)

*Internal to GitLab Only*

- [Prometheus & Grafana](https://drive.google.com/open?id=1pEu4FxYE8gPAMKGaTDOtdMMfoEKjsfBQ)  *(Internal to GitLab only)*
- [GitLab Days](https://drive.google.com/open?id=1LrAW0HI-8SiPzgqCfMCy2mf9XYvkWOKG)  *(Internal to GitLab only)*
- [Executive Business Reviews](https://drive.google.com/open?id=1wQp59jG8uw_UtdNV5vXQjlfC9g5sRD5K)  *(Internal to GitLab only)*
- [Usage Ping Objection Handling](https://docs.google.com/spreadsheets/d/1uCnkIvY4L242QnVTIna7mS3iwJ-Z-Hra_oaqpFhOROQ/edit?ts=5ea74567#gid=0)  *(Internal to GitLab only)* 