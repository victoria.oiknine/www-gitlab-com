---
layout: handbook-page-toc
title: Support Onboarding
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Support Bootcamps
{: #se-bootcamp}

When you first join the team everything will be new to you. Don't worry! In order to get you
started with GitLab quickly, apart from the [General Onboarding Checklist](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/onboarding.md) assigned to you by a People Experience team member on your first day, you will also have a [Support Engineer Onboarding Checklist](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Onboarding%20-%20Support%20Engineer.md) created for you, followed by either a [Support Engineer - Solutions Focus Bootcamp Checklist](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/issue_templates/Onboarding%20-%20Solutions%20Focus.md) or [Support Engineer - GitLab.com Focus Bootcamp Checklist](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Onboarding%20-%20GitLab-dot-com%20Focus.md)
to help guide you through your training.

## Handling tickets
{: #handling-tickets}

### Clearing out Suspended Tickets
{: #clear-suspended}

In Zendesk, various filters send a ticket straight to "suspended" status. This is mostly useful
to remove spam and it works quite well. However, it is possible that actual tickets
are accidentally routed to Suspended Tickets, so it is important to check the new Suspended Ticket queue
at least once a day. Doing this on a regular basis also keeps that queue manageable.