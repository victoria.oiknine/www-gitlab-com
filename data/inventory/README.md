## Inventory Files

The purpose of these inventory files is to provide a standardized way to capture created assets (inventory) which can be used by everyone. A side benefit is that it can also help with metrics collection. This originated as part of the Learn@GitLab project but is being extended to multiple other projects and groups, as the more complete our inventory is the more efficient we can be as a company in using them.

### Layout

The `inventory` folder lives under `/data` of the website and is organized in the following manner to enable scaling to multiple groups. The thought is that each group can be [CODEOWNERS](https://docs.gitlab.com/ee/user/project/code_owners.html) for their own team inventory, but it should still be easy to search through the inventory of "everything" to find what you are looking for.

<pre>
/data/inventory
     |
     ---- learn.yml (Technical Marketing - will change to team name in another MR)
     |
     ---- pmm.yml (Product Marketing)
     |
     ---- ...
     |
     ---- <team>.yml (Team)
</pre>

### Format

The search through the inventory of "everything" to find what you are looking for it is important that all team files use the same format and data fields. This file represents the SSoT for what that format and fields are. If you are making changes make them here first, then make sure everything else follows.

Accepted field descriptions are (fields in bold are required):

<pre>
- title*:                              (the display name of the asset)
   author*:                            (the author of the asset)
   team*:                              (the name of the team that created the asset)
   asset_type*:                        (currently one entry only. Expect this to grow as teams are added. asset type = video, demo)
   date_published*:                    (month and year content first published. In iso-date format. eg 2020-05)
   last_changed                        (date the asset was last changed)
   gitlab_release:                     (major.minor GitLab release # the asset is built about/on. eg 12.10)
   use_case*:                          (use case the asset focuses on. [Derived from customer use case page](https://about.gitlab.com/handbook/use-cases/). Acceptable values are:   )
      - vcc
      - ci
      - cd
      - devsecops
      - agile
      - simplify_devops
      - cloud_native
      - gitops
      - remote
      - other
   stage:                              (multi-select list of stages the asset focuses on. Values should match main entries in [stages.yml](/data/stages.yml))
   category:                           (multi-select list of categories the asset focuses on. Values should match main entries in [categories.yml](/data/categories.yml))
   link*:                              (link to the ungated asset)
   embedded_link:                      (link to embeddable version of asset - typically for videos and demos)
   gated_link:                         (link to the gated asset)
   short_description:                  (a short description of what the asset is about)
   learn:                              (values: true or false or blank. Does this asset show up on learn@gitlab.com. tech marketing team to add this only please)
</pre>
